#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "database.h"

#include <QMainWindow>
#include <QListWidgetItem>

class NoteCheckbox;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    Database *db;
	
	void update_scales();
	void scale_changed();
	
	void select_scale_note(QString);
	void select_scale_type(QString);
	
	void note_checked(bool);
	
	std::vector<NoteCheckbox *> note_checkboxes;
	
	QString int_to_roman(int a)
	{
		QString ans;
		QString M[] = {"","M","MM","MMM"};
		QString C[] = {"","C","CC","CCC","CD","D","DC","DCC","DCCC","CM"};
		QString X[] = {"","X","XX","XXX","XL","L","LX","LXX","LXXX","XC"};
		QString I[] = {"","I","II","III","IV","V","VI","VII","VIII","IX"};
		ans = M[a/1000]+C[(a%1000)/100]+X[(a%100)/10]+I[(a%10)];
		return ans;
	}
};

#endif // MAINWINDOW_H
